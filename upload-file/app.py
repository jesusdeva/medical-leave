import logging
import os
import base64

from flask import Flask, request
from google.cloud import storage

app = Flask(__name__)

CLOUD_STORAGE_BUCKET = 'ugo-api-stage.appspot.com'

@app.route('/upload', methods=['POST'])
def upload():    
    # uploaded_file = request.files.get('file')
    request_data = request.get_json()

    file = None
    fileName = None
   
    if not request_data:
        return 'No file uploaded.', 400

    file = request_data['file']

    fileName = request_data['fileName']

    gcs = storage.Client()

    # Get the bucket that the file will be uploaded to.
    bucket = gcs.get_bucket(CLOUD_STORAGE_BUCKET)

    base64_image = base64.b64decode(file)

    # Create a new blob and upload the file's content.
    blob = bucket.blob(fileName)    

    blob.upload_from_string(
        base64_image,
        content_type='application/pdf'
    )

    blob.make_public()
    
    return blob.public_url